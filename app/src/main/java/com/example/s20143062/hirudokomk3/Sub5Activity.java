package com.example.s20143062.hirudokomk3;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class Sub5Activity extends AppCompatActivity implements AdapterView.OnItemClickListener{

    String[] title = new String[]{};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = getIntent();
        String CATEGORY = intent.getStringExtra("CATEGORY");

        Button sendButton = (Button) findViewById(R.id.button);

        DBOpenHelper dbHelper = new DBOpenHelper(this);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String sql = "SELECT shopname FROM shopinf WHERE category = '" + CATEGORY + "'";

        Cursor c = db.rawQuery(sql, null);
        c.moveToFirst();

        title = new String[c.getCount()+1];

        int count = 0;

        for(int i = 0; i < c.getCount(); i++){
            title[i] = c.getString(0);
            c.moveToNext();
            count = count+1;
        }
        title[count] = "戻る";

        c.close();
        db.close();


        // アダプターを生成
        ListAdapter adapter = (ListAdapter)
                new ArrayAdapter<String>(this,
                        android.R.layout.simple_list_item_1, title);

        //　アダプターをリストビューにセット
        ListView varListView = (ListView) findViewById(R.id.listView);
        varListView.setAdapter(adapter);

        // リストビューを最後の位置までスクロールさせる
        varListView.setSelection(32);

        // イベントリスナーをリストビューにセット
        varListView.setOnItemClickListener(this);

    }


    public void onItemClick(
            AdapterView<?> parent,
            View view, int position, long id) {
        Toast.makeText(this,
                title[position], Toast.LENGTH_SHORT).show();
        if(title[position].equals("戻る")) {
            finish();
        }else{
            ListView list = (ListView)parent;
            String shopname = (String)list.getItemAtPosition(position);
            //intent.putExtra(name, value);
            Intent intent = new Intent(getApplication(), Sub4Activity.class);
            intent.putExtra("SHOPNAME",shopname );
            startActivity(intent);
        }
    }
}
