package com.example.s20143062.hirudokomk3;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class SubActivity extends AppCompatActivity{

    String[] title = new String[]{};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DBOpenHelper dbHelper = new DBOpenHelper(this);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String sql = "SELECT shopname FROM shopinf";

        Cursor c = db.rawQuery(sql, null);
        c.moveToFirst();

        title = new String[c.getCount()];

        int count = 0;

        for(int i = 0; i < c.getCount(); i++){
            title[i] = c.getString(0);
            c.moveToNext();
            count = count+1;
        }

        c.close();
        db.close();

        // 配列からListへ変換します。
        List<String> list= Arrays.asList(title);

        // リストの並びをシャッフルします。
        Collections.shuffle(list);

        // listから配列へ戻します。
        String[] array2 =(String[])list.toArray(new String[list.size()]);

        // シャッフルされた配列の先頭を取得します。
        String shopname = array2[0];

        Intent intent = new Intent(getApplication(), Sub4Activity.class);
        intent.putExtra("SHOPNAME",shopname );
        startActivity(intent);


    }

}

