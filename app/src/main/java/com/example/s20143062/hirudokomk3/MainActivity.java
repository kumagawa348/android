package com.example.s20143062.hirudokomk3;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener{
    // リストビューに表示するデータ
    String[] title = new String[]{"カテゴリー別", "すべて表示","ランダム","終了する"};
    //String[] title = new String[]{};
    String shopname = "";
    String[] shop = new String[]{};




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button sendButton = (Button) findViewById(R.id.button);

        DBOpenHelper dbHelper = new DBOpenHelper(this);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String sql = "SELECT shopname FROM shopinf";

        Cursor c = db.rawQuery(sql, null);
        c.moveToFirst();

        shop = new String[c.getCount()];

        int count = 0;

        for(int i = 0; i < c.getCount(); i++){
            shop[i] = c.getString(0);
            c.moveToNext();
            count = count+1;
        }

        c.close();
        db.close();



        //String sql = "SELECT shopname FROM shopinf";

        //Cursor c = db.rawQuery(sql, null);
        //c.moveToFirst();

        //title = new String[c.getCount()];

        //for(int i = 0; i < c.getCount(); i++){
        //    title[i] = c.getString(0);
        //    c.moveToNext();
        //}

        //c.close();
        //db.close();

        // アダプターを生成
        ListAdapter adapter = (ListAdapter)
                new ArrayAdapter<String>(this,
                        android.R.layout.simple_list_item_1, title);

        //　アダプターをリストビューにセット
        ListView varListView = (ListView) findViewById(R.id.listView);
        varListView.setAdapter(adapter);

        // リストビューを最後の位置までスクロールさせる
        varListView.setSelection(0);

        // イベントリスナーをリストビューにセット
        varListView.setOnItemClickListener(this);

    }

    // コールバックメソッドを利用してトーストを表示
    public void onItemClick(
            AdapterView<?> parent,
            View view, int position, long id) {
        Toast.makeText(this,
           title[position], Toast.LENGTH_SHORT).show();

        Intent intent = new Intent();

        if (title[position].equals("カテゴリー別")) {
            intent.setClassName("com.example.s20143062.hirudokomk3", "com.example.s20143062.hirudokomk3.Sub2Activity");
            startActivity(intent);
        }else if(title[position].equals("すべて表示")) {
            intent.setClassName("com.example.s20143062.hirudokomk3", "com.example.s20143062.hirudokomk3.Sub3Activity");
            startActivity(intent);
        }else if(title[position].equals("ランダム")) {
            // 配列からListへ変換します。
            List<String> list= Arrays.asList(shop);

            // リストの並びをシャッフルします。
            Collections.shuffle(list);

            // listから配列へ戻します。
            String[] array2 =(String[])list.toArray(new String[list.size()]);

            // シャッフルされた配列の先頭を取得します。
            shopname = array2[0];
            intent = new Intent(getApplication(), Sub4Activity.class);
            intent.putExtra("SHOPNAME",shopname );
            startActivity(intent);
        }else if(title[position].equals("終了する")) {
            finish();
        }
    }
}
