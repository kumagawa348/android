package com.example.s20143062.hirudokomk3;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class Sub4Activity extends AppCompatActivity {
    String comment = "";
    String time = "";
    String price = "";
    String address = "";
    String category = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub4);
        Intent intent = getIntent();
        String SHOPNAME = intent.getStringExtra("SHOPNAME");


        DBOpenHelper dbHelper = new DBOpenHelper(this);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String sql = "SELECT comment FROM shopinf WHERE shopname = '" + SHOPNAME + "'";
        Cursor c = db.rawQuery(sql, null);
        c.moveToFirst();
        comment = c.getString(0);

        sql = "SELECT time FROM shopinf WHERE shopname = '" + SHOPNAME + "'";
        c = db.rawQuery(sql, null);
        c.moveToFirst();
        time = c.getString(0);

        sql ="SELECT price FROM shopinf WHERE shopname = '" + SHOPNAME + "'";
        c = db.rawQuery(sql, null);
        c.moveToFirst();
        price = c.getString(0);

        sql = "SELECT address FROM shopinf WHERE shopname = '" + SHOPNAME + "'";
        c = db.rawQuery(sql, null);
        c.moveToFirst();
        address = c.getString(0);

        sql = "SELECT category FROM shopinf WHERE shopname = '" + SHOPNAME + "'";
        c = db.rawQuery(sql, null);
        c.moveToFirst();
        category = c.getString(0);

        c.close();
        db.close();

        TextView text1 = (TextView)findViewById(R.id.textView);
        text1.setText(SHOPNAME);

        TextView text3 = (TextView)findViewById(R.id.textView3);
        text3.setText(category);

        TextView text4 = (TextView)findViewById(R.id.textView4);
        text4.setText(comment);

        TextView text5 = (TextView)findViewById(R.id.textView5);
        text5.setText(address);



    }
}
