package com.example.s20143062.hirudokomk3;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by s20143062 on 2016/10/27.
 */
public class DBOpenHelper  extends SQLiteOpenHelper
    {
    private static final int DATABASE_VERSION = 1;
    public static final String TAG = "DBOpenHelper";
    public Context m_context;
    public static final String DATABASE_NAME = "Shop.db";

    private static final String BOOK_TABLE_CREATE =
            "CREATE TABLE shopinf ( " +
                    "shopno  INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "shopname TEXT NOT NULL, " +
                    "category text, " +
                    "comment TEXT, " +
                    "time INTEGER, " +
                    "price INTEGER, " +
                    "address TEXT);";

    public DBOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.m_context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(BOOK_TABLE_CREATE);

        db.execSQL("INSERT INTO shopinf(shopname, category, comment, address) values ('エル・コーヒーショップ','カレー','カレーのトッピングのバリュエーションが素晴らしい。','札幌市白石区菊水６条３丁目４−２０ レストハウス吉田 1F');");
        db.execSQL("INSERT INTO shopinf(shopname, category, comment, address) values ('ビストロ・プティット・レジョン','レストラン','本格的なフランス料理が食べることができます。お洒落なお店でもあるので女性にお勧め！','札幌市白石区菊水５条３丁目５−１１');");
        db.execSQL("INSERT INTO shopinf(shopname, category, comment, address) values ('茶房かのん','デザート','非常におしゃれなカフェでパンケーキのボリュームがすごい！','札幌市白石区菊水３条５丁目５−１８');");
        db.execSQL("INSERT INTO shopinf(shopname, category, comment, address) values ('麺屋おざわ','ラーメン','醤油と味噌を取り扱うお店です。学校からすごい近い！','札幌市白石区菊水６条３丁目１−２２ ISコート 1F');");
        db.execSQL("INSERT INTO shopinf(shopname, category, comment, address) values ('中華そば カリフォルニア','ラーメン','あっさりとしたスープを取り扱う人気店。一度は行っておきたいお店。','札幌市 白石区菊水３条４丁目４−６ ビバ菊水 1F');");
        db.execSQL("INSERT INTO shopinf(shopname, category, comment, address) values ('味の東一','ラーメン','ボリューミーでがっつり食べたい人にはお勧め！','北海道札幌市白石区菊水７条４丁目４−２');");
        db.execSQL("INSERT INTO shopinf(shopname, category, comment, address) values ('株式会社北海道ドルチェ','デザート','マフィンやケーキを取り揃えているので甘いもの食べたかったらここ！','北海道札幌市白石区菊水２条３丁目１−２５');");
        db.execSQL("INSERT INTO shopinf(shopname, category, comment, address) values ('らあめんがんてつ','ラーメン','味噌ラーメンならここ！昼割りとかもあるのでお勧め！','北海道札幌市白石区東札幌４条１丁目１−１-１ イーアス札幌２F');");
        db.execSQL("INSERT INTO shopinf(shopname, category, comment, address) values ('サイゼリア','レストラン','とにかくコスパが良い！学生に嬉しいイタリアンレストラン','北海道札幌市白石区東札幌３条１丁目１−１−１ イーアス札幌１F');");
        db.execSQL("INSERT INTO shopinf(shopname, category, comment, address) values ('麻布茶房','デザート','和風甘味所といえばここ！和風パフェを取り揃えている！','北海道札幌市白石区東札幌３条１丁目１−１−１ イーアス札幌1F');");
        db.execSQL("INSERT INTO shopinf(shopname, category, comment, address) values ('イタリアン・トマト・カフェジュニア','レストラン','パスタが右に出るものがいないほど美味しい！','北海道札幌市白石区東札幌３条１丁目１−１−１ イーアス札幌1F');");
        db.execSQL("INSERT INTO shopinf(shopname, category, comment, address) values ('サブウェイ','ファストフード','色々なサンドイッチを提供してバリュエーションが豊富！','北海道札幌市白石区東札幌３条１丁目１−１−１ イーアス札幌1F');");
        db.execSQL("INSERT INTO shopinf(shopname, category, comment, address) values ('JoJos','カフェ','ロッククライミングがある変わったカフェ。スコーンが美味しい！','北海道札幌市白石区東札幌３条１丁目１−１−１ イーアス札幌1F');");
        db.execSQL("INSERT INTO shopinf(shopname, category, comment, address) values ('どんぶりくん','丼物','牛丼、親子丼など丼物ならなんでも揃っている！','北海道札幌市白石区東札幌３条１丁目１−１−１ イーアス札幌1F');");
        db.execSQL("INSERT INTO shopinf(shopname, category, comment, address) values ('そば処　千家','そば・うどん','ボリューム満点の定食が美味しい！','北海道札幌市白石区東札幌３条１丁目１−１−１ イーアス札幌2F');");
        db.execSQL("INSERT INTO shopinf(shopname, category, comment, address) values ('ハニーハニー','デザート','本格派クレープを取り揃えていてバリュエーションが豊富！','海道札幌市白石区東札幌３条１丁目１−１−１ イーアス札幌1F');");
        db.execSQL("INSERT INTO shopinf(shopname, category, comment, address) values ('回転寿司○海','寿司','寿司を食べてちょっと贅沢をしたいときはここ！','海道札幌市白石区東札幌３条１丁目１−１−１ イーアス札幌２F');");
        db.execSQL("INSERT INTO shopinf(shopname, category, comment, address) values ('北京厨房　京花楼','中華','1000円で本格中華がセットで楽しめる！上品な味わいがポイントです','海道札幌市白石区東札幌３条１丁目１−１−１ イーアス札幌２F');");
        db.execSQL("INSERT INTO shopinf(shopname, category, comment, address) values ('北海道純雪うどん','そば・うどん','コシのある麺と香ばしい汁がやみつきになる！','海道札幌市白石区東札幌３条１丁目１−１−１ イーアス札幌1F');");
        db.execSQL("INSERT INTO shopinf(shopname, category, comment, address) values ('ラビデカフェ','カレー','お店のこだわりが感じさせられるスープカレーを提供している','海道札幌市白石区東札幌３条１丁目１−１−１ イーアス札幌２');");
        db.execSQL("INSERT INTO shopinf(shopname, category, comment, address) values ('コメダ珈琲店','カフェ','お店の雰囲気がとても良くお昼以外にゆったりする時にもお勧め','北海道札幌市白石区東札幌５条１丁目３ 東札幌5条1丁目3番1号');");
        db.execSQL("INSERT INTO shopinf(shopname, category, comment, address) values ('ロッテリア','ファストフード','お馴染みのチェーン店で少し高いが味は確証出来るほど美味しい',' 北海道札幌市白石区東札幌３条２丁目１−５');");
        db.execSQL("INSERT INTO shopinf(shopname, category, comment, address) values ('ミスタードーナツ','デザート','安くて美味しく安定感が素晴らしい','北海道札幌市白石区菊水３条５丁目２−１−１');");
        db.execSQL("INSERT INTO shopinf(shopname, category, comment, address) values ('BAKED MUFF','カフェ','可愛い外観が特徴。焼き菓子が豊富である','北海道札幌市白石区菊水２条２丁目３－１７');");
        db.execSQL("INSERT INTO shopinf(shopname, category, comment, address) values ('究そば','ラーメン','メインはラーメンだがもちろんそばも置いてある！','北海道札幌市白石区菊水２条２丁目３−２３');");
        db.execSQL("INSERT INTO shopinf(shopname, category, comment, address) values ('尾州鮨','寿司','いわゆる回らない寿司のお店。高いが味は保障出来る旨さ。','北海道札幌市白石区東札幌４条２丁目４');");
        db.execSQL("INSERT INTO shopinf(shopname, category, comment, address) values ('網取物語','ラーメン','海鮮ラーメンが食べたいときはここ！味も濃厚でたまらない！','北海道札幌市白石区菊水３条３丁目２−１５ ユーエイコーポラス1F');");
        db.execSQL("INSERT INTO shopinf(shopname, category, comment, address) values ('セブンイレブン','コンビニ','美味しい弁当やおにぎりを求めるならここ！安いカップ麺も取り揃えている','北海道札幌市白石区菊水７条４丁目３ 白石区菊水７条４丁目３−１');");
        db.execSQL("INSERT INTO shopinf(shopname, category, comment, address) values ('ローソン','コンビニ','からあげくんやLチキなどのフライヤーがおすすめ！','北海道札幌市白石区白石区東札幌２条１丁目１−２５ 東札幌 ２ 条 １‐１‐２５ ドエル東札幌');");
        db.execSQL("INSERT INTO shopinf(shopname, category, comment, address) values ('セイコーマート','コンビニ','学生の味方はここ！とにかく安いのが特徴！！！','北海道札幌市白石区菊水８条３丁目１－３');");
        db.execSQL("INSERT INTO shopinf(shopname, category, comment, address) values ('サンクス','コンビニ','スイーツに凄く力を入れているので甘いものを求めるならここ！','北海道札幌市白石区菊水６条２丁目９ 白石区菊水6条2丁目9−17');");
    }
        //ラーメン　フランス料理　コンビニ　レストラン

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
